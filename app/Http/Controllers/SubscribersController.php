<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribersRequest;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $page_title  = 'Subscriber List';
        $subscribers = Subscriber::latest()->simplePaginate(10);
        return view('subscribers.index', compact('page_title', 'subscribers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Add New Subscriber';
        return view('subscribers.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubscribersRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SubscribersRequest $request)
    {
        Subscriber::create($request->except('_token'));
        return redirect()->route('subscribers.index')->with('toast_success', 'Subscriber created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Subscriber";
        $subscriber = Subscriber::findOrFail($id);
        return view('subscribers.edit', compact('page_title', 'subscriber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SubscribersRequest $request
     * @param Subscriber $subscriber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SubscribersRequest $request, Subscriber $subscriber)
    {
        $subscriber->update($request->except('_token'));
        return redirect()->route('subscribers.index')->with('toast_success', 'Subscriber updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subscriber $subscriber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Subscriber $subscriber)
    {
        $subscriber->delete();
        return redirect()->route('subscribers.index')->with('toast_success', 'Subscriber deleted successfully');
    }
}
