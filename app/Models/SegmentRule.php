<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SegmentRule extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function segment()
    {
        return $this->belongsTo(Segment::class, 'segment_id')->withDefault();
    }
}
