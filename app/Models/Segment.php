<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function segment_rules()
    {
        return $this->hasMany(SegmentRule::class, 'segment_id');
    }

}
