<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    use HasFactory;

    // Operator Types
    const TEXT = 'text';
    const DATE = 'date';
    public static $operatorTypes = [self::TEXT, self::DATE];

    protected $guarded = ['id'];

    public $timestamps = true;
}
