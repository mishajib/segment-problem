<?php

namespace Database\Seeders;

use App\Models\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operator::truncate();
        $logicOperators = [
            [
                'operator_type'  => 'text',
                'logic_operator' => 'is',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'is_not',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'starts_with',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'ends_with',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'contains',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'doesnot_starts_with',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'doesnot_end_with',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'text',
                'logic_operator' => 'doesnot_contains',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'before',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'after',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'on',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'on or before',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'on or after',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
            [
                'operator_type'  => 'date',
                'logic_operator' => 'between',
                'created_at'     => now(),
                'updated_at'     => now(),
            ],
        ];

        Operator::insert($logicOperators);

        $this->command->info('Logic operators created successfully');
    }
}
