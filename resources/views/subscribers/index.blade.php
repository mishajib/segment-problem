<x-app :pageTitle="$page_title">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                {{ $page_title }}
                <a href="{{ route('subscribers.create') }}" class="btn btn-primary float-right">
                    Add New Subscriber
                </a>
            </h3>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#SL</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Birth Day</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($subscribers as $key => $subscriber)
                    <tr>
                        <th scope="row">{{ $subscribers->firstItem() + $key }}</th>
                        <td>{{ $subscriber->first_name }}</td>
                        <td>{{ $subscriber->last_name }}</td>
                        <td>{{ $subscriber->email }}</td>
                        <td>{{ \Carbon\Carbon::parse($subscriber->birth_day)->format('F j, Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($subscriber->create_at)->format('F j, Y - g:ia') }}</td>
                        <td>
                            <a href="{{ route('subscribers.edit', $subscriber->id) }}"
                               class="btn btn-info btn-sm">
                                Edit
                            </a>

                            <x-form-button class="btn btn-danger btn-sm"
                                           action="{{ route('subscribers.destroy', $subscriber->id) }}" method="DELETE"
                                           formid="delete-form-{{ $subscriber->id }}" style="display: inline-block"
                                           onclick="makeDeleteRequest(event, {{ $subscriber->id }})">
                                Delete
                            </x-form-button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center text-danger">
                            No subscribers found!
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <nav aria-label="Page navigation example">
                <ul class="pagination float-right">
                    {{ $subscribers->links() }}
                </ul>
            </nav>
        </div>
    </div>
</x-app>
