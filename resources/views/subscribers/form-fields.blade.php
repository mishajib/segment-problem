<div class="card-body">
    <div class="form-group row">
        <div class="col-6">
            <label for="first-name">First Name</label>
            <input type="text" id="first-name" class="form-control" name="first_name"
                   placeholder="Enter first name"
                   value="{{ $subscriber->first_name ?? old('first_name') }}" autofocus>
        </div>
        <div class="col-6">
            <label for="last-name">Last Name</label>
            <input type="text" id="last-name" class="form-control" name="last_name"
                   placeholder="Enter last name"
                   value="{{ $subscriber->last_name ?? old('last_name') }}">
        </div>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" id="email" class="form-control" name="email" placeholder="Enter email"
               value="{{ $subscriber->email ?? old('email') }}">
    </div>
    <div class="form-group">
        <label for="birth-date">Birth Date</label>
        <input type="date" id="birth-date" class="form-control" name="birth_day"
               value="{{ request()->routeIs('subscribers.edit') ? \Carbon\Carbon::parse($subscriber->birth_day)->format('Y-m-d') : '' ?? old('birth_day') }}">
    </div>
</div>

<div class="card-footer pb-5">
    <button type="submit" class="btn btn-primary float-right ml-1">
        {{ request()->routeIs('subscribers.create') ? 'Save' : 'Update' }}
    </button>
    <a href="{{ route('subscribers.index') }}" class="btn btn-dark float-right">Back</a>
</div>
