<div class="card-body">
    <div class="form-group">
        <label for="segment-name">Segment Name</label>
        <input type="text" id="segment-name" class="form-control" name="segment_name"
               placeholder="Enter segment name"
               value="{{ $segment->segment_name ?? old('segment_name') }}" autofocus>
    </div>
    <fieldset>
        <legend>
            Segment Logic
        </legend>
        <div class="segment-group">
            <div class="card segment-element" style="border: 1px solid grey; padding: 10px">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button"
                                class="btn btn-danger float-right btn-sm remove-segment-logic-btn">
                            X
                        </button>
                    </div>
                </div>

                <div class="date-field-group">
                    <div class="date-field-element">
                        <div class="form-group row">
                            <div class="col-3">
                                <select name="and[0][date_logics][0][type]" id="date-type0" class="form-control">
                                    <option value="">Select Date Type</option>
                                    <option value="created_at">Created at</option>
                                    <option value="birth_day">Birth Day</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <select name="and[0][date_logics][0][operator]" id="date-logic-operator0"
                                        class="form-control">
                                    <option value="">Select Logic Operator</option>
                                    @forelse($operators as $dateOperator)
                                        @if($dateOperator->operator_type == 'date')
                                            <option value="{{ $dateOperator->logic_operator }}">
                                                {{ $dateOperator->logic_operator }}
                                            </option>
                                        @endif
                                    @empty
                                        <option value="" disabled class="text-danger">
                                            No date type operators found!
                                        </option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-3">
                                <input type="date" id="date0" class="form-control" name="and[0][date_logics][0][date]">
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-danger btn-sm remove-date-btn">
                                    Delete
                                </button>
                                <button type="button" class="btn btn-primary btn-sm add-date-btn"
                                        onclick="dateLogicRepeater(0)">
                                    + Or
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="text-group">
                    <div class="text-element">
                        <div class="form-group row">
                            <div class="col-3">
                                <select name="and[0][text_logics][0][type]" id="text-type0" class="form-control">
                                    <option value="">Select Text Type</option>
                                    <option value="first_name">First name</option>
                                    <option value="last_name">Last name</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <select name="and[0][text_logics][0][operator]" id="text-logic-operator0"
                                        class="form-control">
                                    <option value="">Select Logic Operator</option>
                                    @forelse($operators as $textOperator)
                                        @if($textOperator->operator_type == 'text')
                                            <option value="{{ $textOperator->logic_operator }}">
                                                {{ $textOperator->logic_operator }}
                                            </option>
                                        @endif
                                    @empty
                                        <option value="" disabled class="text-danger">
                                            No text type operators found!
                                        </option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-3">
                                <input type="text" id="text0" class="form-control" name="and[0][text_logics][0][text]"
                                       placeholder="Enter text">
                            </div>
                            <div class="col-3">
                                <button type="button" class="btn btn-danger btn-sm remove-text-btn">
                                    Delete
                                </button>
                                <button type="button" class="btn btn-primary btn-sm add-text-btn"
                                        onclick="textLogicRepeater(0)">
                                    + Or
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <br>
        </div>


        <div class="form-group mt-n2">
            <button class="btn btn-primary float-right" type="button" id="add-segment-logic">
                + And
            </button>
        </div>
    </fieldset>
</div>

<div class="card-footer pb-5">
    <button type="submit" class="btn btn-primary float-right ml-1">
        {{ request()->routeIs('subscribers.create') ? 'Save' : 'Update' }}
    </button>
    <a href="{{ route('subscribers.index') }}" class="btn btn-dark float-right">Back</a>
</div>
