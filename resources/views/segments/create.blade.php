<x-app :pageTitle="$page_title">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                {{ $page_title }}
            </h3>
        </div>
        <x-form action="{{ route('segments.store') }}" method="POST">
            @include('segments.form-fields')
        </x-form>
    </div>

    <x-slot name="scripts">
        <script>
            let operators = @json($operators);

            function segmentRepeater() {
                let segmentElementCount = $('.segment-element').length;
                let dateOperatorOptions = '';
                let textOperatorOptions = '';
                $('.remove-segment-logic-btn').click(function () {
                    if (confirm('Are you sure?')) {
                        segmentElementCount--;
                        $(this).parent().parent().parent().remove();
                    }
                });
                $('#add-segment-logic').click(function () {
                    let elementData = `
                        <div class="card segment-element" style="border: 1px solid grey; padding: 10px">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button"
                                class="btn btn-danger float-right btn-sm remove-segment-logic-btn">
                            X
                        </button>
                    </div>
                </div>

                <div class="date-field-group">
                    <div class="date-field-element">
                        <div class="form-group row">
                            <div class="col-3">
                                <select name="and[${segmentElementCount}][date_logics][0][type]" id="date-type${segmentElementCount}" class="form-control">
                                    <option value="">Select Date Type</option>
                                    <option value="created_at">Created at</option>
                                    <option value="birth_day">Birth Day</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <select name="and[${segmentElementCount}][date_logics][0][operator]" id="date-logic-operator${segmentElementCount}" class="form-control">
                                    <option value="">Select Logic Operator</option>

                                </select>
                            </div>
              <div class="col-3">
                  <input type="date" id="date${segmentElementCount}" class="form-control" name="and[${segmentElementCount}][date_logics][0][date]">
              </div>
              <div class="col-3">
                  <button type="button" class="btn btn-danger btn-sm remove-date-btn">
                      Delete
                  </button>
                  <button type="button" class="btn btn-primary btn-sm add-date-btn" onclick="dateLogicRepeater(${segmentElementCount})">
                      + Or
                  </button>
              </div>
          </div>
      </div>
  </div>

  <br>

  <div class="text-group">
      <div class="text-element">
          <div class="form-group row">
              <div class="col-3">
                  <select name="and[${segmentElementCount}][text_logics][0][type]" id="text-type${segmentElementCount}" class="form-control">
                      <option value="">Select Text Type</option>
                      <option value="first_name">First name</option>
                      <option value="last_name">Last name</option>
                      <option value="email">Email</option>
                  </select>
              </div>
              <div class="col-3">
                  <select name="and[${segmentElementCount}][text_logics][0][operator]" id="text-logic-operator${segmentElementCount}" class="form-control">
                      <option value="">Select Logic Operator</option>
                  </select>
              </div>
              <div class="col-3">
                  <input type="text" id="text${segmentElementCount}" class="form-control" name="and[${segmentElementCount}][text_logics][0][text]"
                         placeholder="Enter text">
              </div>
              <div class="col-3">
                  <button type="button" class="btn btn-danger btn-sm remove-text-btn">
                      Delete
                  </button>
                  <button type="button" class="btn btn-primary btn-sm add-text-btn" onclick="textLogicRepeater(${segmentElementCount})">
                      + Or
                  </button>
              </div>
          </div>
      </div>
  </div>

</div>
<br>
`;
                    $('.segment-group').append(elementData);
                    $('.remove-segment-logic-btn').click(function () {
                        if (confirm('Are you sure?')) {
                            segmentElementCount--;
                            $(this).parent().parent().parent().remove();
                        }
                    });
                    $('.remove-date-btn').click(function () {
                        if (confirm('Are you sure?')) {
                            dateFieldCount--;
                            $(this).parent().parent().parent().remove();
                        }
                    });
                    $('.remove-text-btn').click(function () {
                        if (confirm('Are you sure?')) {
                            textFieldCount--;
                            $(this).parent().parent().parent().remove();
                        }
                    });
                    segmentElementCount++;
                });
            }

            function dateLogicRepeater(and_index) {
                let dateFieldCount = $('.date-field-element').length;
                let operatorOptions = '';
                operators.forEach(item => {
                    if (item.operator_type === 'date') {
                        operatorOptions += `<option value="${item.logic_operator}">${item.logic_operator}</option>`;
                    }
                });

                let dateFieldElementData = `
                        <div class="date-field-element">
                        <div class="form-group row">
                            <div class="col-3">
                                <select name="and[${and_index}][date_logics][${dateFieldCount}][type]" id="date-type${dateFieldCount}" class="form-control">
                                    <option value="">Select Date Type</option>
                                    <option value="created_at">Created at</option>
                                    <option value="birth_day">Birth Day</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <select name="and[${and_index}][date_logics][${dateFieldCount}][operator]" id="date-logic-operator${dateFieldCount}" class="form-control">
                                    <option value="">Select Logic Operator</option>
                                    ${operatorOptions}

                   </select>
               </div>
               <div class="col-3">
                   <input type="date" id="date${dateFieldCount}" class="form-control" name="and[${and_index}][date_logics][${dateFieldCount}][date]">
               </div>
               <div class="col-3">
                   <button type="button" class="btn btn-danger btn-sm remove-date-btn">
                       Delete
                   </button>
                   <button type="button" class="btn btn-primary btn-sm add-date-btn"
                           onclick="dateLogicRepeater(${and_index})">
                       + Or
                   </button>
               </div>
           </div>
       </div>
`;
                $('.date-field-group').append(dateFieldElementData);
                $('.remove-date-btn').click(function () {
                    if (confirm('Are you sure?')) {
                        dateFieldCount--;
                        $(this).parent().parent().parent().remove();
                    }
                });
                dateFieldCount++;
            }

            function textLogicRepeater(and_index) {
                let textFieldCount = $('.text-element').length;
                let operatorOptions = '';
                operators.forEach(item => {
                    if (item.operator_type === 'text') {
                        operatorOptions += `<option value="${item.logic_operator}">${item.logic_operator}</option>`;
                    }
                });
                let textElementData = `
                        <div class="text-element">
                        <div class="form-group row">
                            <div class="col-3">
                                <select name="and[${and_index}][text_logics][${textFieldCount}][type]" id="text-type${textFieldCount}" class="form-control">
                                    <option value="">Select Text Type</option>
                                    <option value="first_name">First name</option>
                                    <option value="last_name">Last name</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <select name="and[${and_index}][text_logics][${textFieldCount}][operator]" id="text-logic-operator${textFieldCount}" class="form-control">
                                    <option value="">Select Logic Operator</option>
                                    ${operatorOptions}

                    </select>
                </div>
                <div class="col-3">
                    <input type="text" id="text${textFieldCount}" class="form-control" name="and[${and_index}][text_logics][${textFieldCount}][text]"
                           placeholder="Enter text">
                </div>
                <div class="col-3">
                    <button type="button" class="btn btn-danger btn-sm remove-text-btn">
                        Delete
                    </button>
                    <button type="button" class="btn btn-primary btn-sm add-text-btn"
                            onclick="textLogicRepeater(${and_index})">
                        + Or
                    </button>
                </div>
            </div>
        </div>
`;
                $('.text-group').append(textElementData);

                $('.remove-text-btn').click(function () {
                    if (confirm('Are you sure?')) {
                        textFieldCount--;
                        $(this).parent().parent().parent().remove();
                    }
                });
                textFieldCount++;
            }

            $(document).ready(function () {
                segmentRepeater();


                $('.remove-date-btn').click(function () {
                    if (confirm('Are you sure?')) {
                        dateFieldCount--;
                        $(this).parent().parent().parent().remove();
                    }
                });
                $('.remove-text-btn').click(function () {
                    if (confirm('Are you sure?')) {
                        textFieldCount--;
                        $(this).parent().parent().parent().remove();
                    }
                });


            });
        </script>
    </x-slot>
</x-app>
