<x-app :pageTitle="$page_title">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                {{ $page_title }}
            </h3>
        </div>
        <x-form action="{{ route('subscribers.update', $subscriber->id) }}" method="POST">
            @include('subscribers.form-fields')
        </x-form>
    </div>
</x-app>
