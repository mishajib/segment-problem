<x-app :pageTitle="$page_title">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                {{ $page_title }}
                <a href="{{ route('segments.create') }}" class="btn btn-primary float-right">
                    Add New Segment
                </a>
            </h3>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#SL</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Birth Day</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($segments as $segment)
                    <tr>
                        <th scope="row">{{ $segments->firstItem() + 1 }}</th>
                        <td>{{ $segment->first_name }}</td>
                        <td>{{ $segment->last_name }}</td>
                        <td>{{ $segment->email }}</td>
                        <td>{{ \Carbon\Carbon::parse($segment->birth_day)->format('F j, Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($segment->create_at)->format('F j, Y - g:ia') }}</td>
                        <td>
                            <a href="{{ route('segments.edit', $segment->id) }}"
                               class="btn btn-info btn-sm">
                                Edit
                            </a>

                            <x-form-button class="btn btn-danger btn-sm"
                                           action="{{ route('segments.destroy', $segment->id) }}" method="DELETE"
                                           formid="delete-form-{{ $segment->id }}" style="display: inline-block"
                                           onclick="makeDeleteRequest(event, {{ $segment->id }})">
                                Delete
                            </x-form-button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center text-danger">
                            No segments found!
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <nav aria-label="Page navigation example">
                <ul class="pagination float-right">
                    {{ $segments->links() }}
                </ul>
            </nav>
        </div>
    </div>
</x-app>
