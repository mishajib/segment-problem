<x-form method="{{ $method }}" action="{{ $action }}" class="d-inline" id="{{ $formid ?? '' }}">
    <button type="submit" {{ $attributes }}>
        {{ $slot }}
    </button>
</x-form>
