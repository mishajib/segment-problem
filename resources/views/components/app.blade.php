<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pageTitle ?? '' }} | Segment Problem</title>

    <link href="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">

    {{ $styles ?? '' }}
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
                <a class="navbar-brand" href="{{ route('subscribers.index') }}">Segment Problem</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ request()->routeIs('subscribers.*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('subscribers.index') }}">Subscribers</a>
                        </li>
                        <li class="nav-item {{ request()->routeIs('segments.*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('segments.index') }}">Segments</a>
                        </li>
                    </ul>
                </div>
            </nav>
            {{ $slot ?? '' }}
        </div>
    </div>
</div>


<script src="//code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@include('errors.sweetalert')

{{ $scripts ?? '' }}
</body>
</html>
