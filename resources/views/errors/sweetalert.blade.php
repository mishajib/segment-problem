@if(Session::has('success'))
    <script>
        Swal.fire(
            'Success!',
            '{{ Session::get('success') }}',
            'success'
        )
    </script>
@endif

@if(Session::has('error'))
    <script>
        Swal.fire(
            'Error!',
            '{{ Session::get('error') }}',
            'error'
        )
    </script>
@endif

@if(Session::has('info'))
    <script>
        Swal.fire(
            'Info!',
            '{{ Session::get('info') }}',
            'info'
        )
    </script>
@endif

@if(Session::has('warning'))
    <script>
        Swal.fire(
            'Warning!',
            '{{ Session::get('warning') }}',
            'warning'
        )
    </script>
@endif

@if($errors->any())
    @foreach($errors->all() as $error)
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                icon: 'error',
                title: '{{ $error }}'
            });
            /*Swal.fire(
                'Error!',
                '{{ $error }}',
                'error'
            )*/
        </script>
    @endforeach
@endif

{{--Toastr--}}
@if(Session::has('toast_success'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        Toast.fire({
            icon: 'success',
            title: '{{ Session::get('toast_success') }}'
        });
    </script>
@endif

@if(Session::has('toast_error'))
    <script>
        Swal.fire(
            'Error!',
            '{{ Session::get('error') }}',
            'error'
        )
    </script>
@endif

@if(Session::has('toast_info'))
    <script>
        Swal.fire(
            'Info!',
            '{{ Session::get('info') }}',
            'info'
        )
    </script>
@endif

@if(Session::has('toast_warning'))
    <script>
        Swal.fire(
            'Warning!',
            '{{ Session::get('warning') }}',
            'warning'
        )
    </script>
@endif
