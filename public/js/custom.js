function makeDeleteRequest(event, id) {
    event.preventDefault();
    Swal.fire({
        title: 'Are you sure?',
        text: "You will not be able to recover!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            event.preventDefault();
            $('#delete-form-' + id).submit();
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
                'Cancelled',
                'Your data is safe :)',
                'error'
            );
        }
    });
}
